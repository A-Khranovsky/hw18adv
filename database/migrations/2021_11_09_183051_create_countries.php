<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->id();
            $table->char('code', 2)->unique();
            $table->string('name');
            $table->timestamps();
            $table->foreignId('continent_id')->unsigned()->nullable();
            $table->foreign('continent_id')->references('id')
                ->on('continents')->onDelete('SET NULL');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->foreignId('country_id')->unsigned()->nullable();
            $table->foreign('country_id')->references('id')
                ->on('countries')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('users', function (Blueprint $table) {
            $table->dropConstrainedForeignId('country_id');
        });

        Schema::dropIfExists('countries');
    }
}
