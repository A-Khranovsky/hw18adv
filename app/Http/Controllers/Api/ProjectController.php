<?php


namespace App\Http\Controllers\Api;

use App\Http\Resources\ProjectCollection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Models\Project;

class ProjectController
{
    public function Create(Request $request)
    {
        Project::create([
            'name' => $request->get('name')
        ]);
        return response('Project was successfully created', 200);
    }

    public function List(Request $request)
    {
        $query = Project::query();

        if ($request->has('email')) {
            $email = $request->get('email');
            $query->whereHas('users', function (Builder $qb) use ($email) {
                $qb->where('email', $email);
            });
        }

        if ($request->has('continent')) {
            $continent = $request->get('continent');
            $query->whereHas('users', function (Builder $qb) use ($continent) {
                $qb->whereHas('country', function (Builder $qb) use ($continent) {
                    $qb->whereHas('continent', function (Builder $qb) use ($continent) {
                        $qb->where('code', $continent);
                    });
                });
            });
        }

        if ($request->has('label')) {
            $label = $request->get('label');
            $query->whereHas('labels', function (Builder $qb) use ($label) {
                $qb->where('name', $label);
            });
        }
        return new ProjectCollection($query->get());
    }

    public function linkUser($project_id, Request $request)
    {
        $project = Project::find($project_id);
        $ids = $request->get('id');
        $project->users()->sync($ids);

        return response('Project id = ' . $project_id . ' was successfully linked to users id = '
            . implode(',', $ids), 200);
    }

    public function destroy(Request $request) {
        $ids = $request->get('id');
        $projects = Project::whereIn('id', $ids)->get();

        foreach($projects as $project)
        {
            $project->users()->detach();
            $project->labels()->detach();
            $project->delete();
        }

        return response('Projects (id = ' . implode(',', $ids) . ') was successfully deleted', 200);
    }
}
