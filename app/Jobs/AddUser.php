<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AddUser implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $Name;
    protected $Email;
    protected $Verified;
    protected $Country;

    /**
     * Create a new job instance.
     *
     * @return void
     */
//    public function __construct($Name, $Email, $Verified, $Country)
//    {
//        $this->Name = $Name;
//        $this->Email = $Email;
//        $this->Verified = $Verified;
//        $this->Country = $Country;
//    }

    public function __construct($Name)
    {
        $this->Name = $Name;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //AddUser::dispatch()
    }
}
